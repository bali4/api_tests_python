from http import HTTPStatus

import pytest

import data


class TestSimpleSoap:

    def test_simple_soap_req(self, get_simple_soap_call):
        response = get_simple_soap_call(data=data.DIVIDE_DATA)
        assert response.status_code == HTTPStatus.OK
