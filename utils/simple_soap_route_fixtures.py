import pytest

from constants import *
from data import DIVIDE_DATA


@pytest.fixture(scope="class")
def get_simple_soap_call(soap_client):
    def make_get_simple_soap_call(data=None):
        data = data
        headers = {
            'Content-Type': 'text/xml; charset=utf-8',
            'SOAPAction': 'http://tempuri.org/Divide'
        }
        response = soap_client.post_req(url=SIMPLE_REQUEST, headers=headers, form_data=data)
        return response

    return make_get_simple_soap_call
