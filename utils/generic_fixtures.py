import pytest

from client.soap_client import SoapClient


@pytest.fixture(scope="session", autouse=True)
def soap_client(env: str):
    soap_client = SoapClient(env)
    return soap_client
