import configparser
import re

import requests

from constants import URL_FILE


class SoapClient:
    def __init__(self, env: str):
        config_loader = configparser.ConfigParser()
        config_loader.read_file(open(URL_FILE))
        self.env = env
        self.service_url = config_loader.get(env, "base_url")

    def get_req(self, url, headers=None, payload=None, path_params=None, form_data=None, file=None,
                allow_redirect=True):
        response = requests.get(url=self.build_url(self.service_url, url),
                                data=form_data,
                                headers=headers,
                                json=payload,
                                params=path_params,
                                files=file,
                                allow_redirects=allow_redirect
                                )
        return response

    def post_req(self, url, headers=None, payload=None, path_params=None, form_data=None, file=None,
                 allow_redirect=True):
        response = requests.post(url=self.build_url(self.service_url, url),
                                 data=form_data,
                                 headers=headers,
                                 json=payload,
                                 params=path_params,
                                 files=file,
                                 allow_redirects=allow_redirect
                                 )
        return response

    def put_req(self, url, headers=None, payload=None, path_params=None, form_data=None, file=None,
                allow_redirect=True):
        response = requests.put(url=self.build_url(self.service_url, url),
                                data=form_data,
                                headers=headers,
                                json=payload,
                                params=path_params,
                                files=file,
                                allow_redirects=allow_redirect
                                )
        return response

    def patch_req(self, url, headers=None, payload=None, path_params=None, form_data=None, file=None,
                  allow_redirect=True):
        response = requests.patch(url=self.build_url(self.service_url, url),
                                  data=form_data,
                                  headers=headers,
                                  json=payload,
                                  params=path_params,
                                  files=file,
                                  allow_redirects=allow_redirect
                                  )
        return response

    def delete_req(self, url, headers=None, payload=None, path_params=None, form_data=None, file=None,
                   allow_redirect=True):
        response = requests.delete(url=self.build_url(self.service_url, url),
                                   data=form_data,
                                   headers=headers,
                                   json=payload,
                                   params=path_params,
                                   files=file,
                                   allow_redirects=allow_redirect
                                   )
        return response

    @staticmethod
    def build_url(base_url, tail_end):
        if re.match("http[s]?:", tail_end):
            return tail_end

        return base_url + tail_end
