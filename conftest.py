import pytest
from utils.generic_fixtures import *
from utils.simple_soap_route_fixtures import *

def pytest_addoption(parser):
    parser.addoption(
        "--env",
        action="store",
        default="qa",
        help="Environments to run test: dev/qa/prod"
    )


@pytest.fixture(scope="session")
def env(pytestconfig):
    valid_envs = ["qa", "dev", "prod"]
    given_env = pytestconfig.getoption("env")
    assert given_env in valid_envs, "Not a valid environment - Valid environments: qa, dev or prod"
    return given_env
