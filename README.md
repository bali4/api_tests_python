# API Tests Python POC

POC for API automation testing with python/pytest

## Prerequisites:

```
Python
Pytest
Allure-Pytest
```

## Tests Execution:

To execute the tests locally


1- Clone by : 
```
git clone git@gitlab.com:bali4/api_tests_python.git
```
2- `cd` to `api_tests_python` directory

3- Execute command in terminal:

```
pytest -v -s --alluredir=./reports --env=<env to test>
```
*Required command-line arguments:*

*--env*

`--env` one of `dev, qa, and prod` arguments and set the base URL to the correct environment.  
Any new environment needs to be added to 

*_e.g_*

```pytest -v --env=qa```

4- To view the allure test report
```
allure serve ./reports
```
